--- 
layout: markdown_page
title: "How to build a remote team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How it works at GitLab

For GitLab, being an all-remote company did not start as an intentional decision. It was a natural evolution as our first team members started choosing to work from home. 
Traditional, on-site companies often take processes, camaraderie, and culture for granted and allow it to develop organically. 
But in an all-remote company, you have to organize it. This can be hard to do at first, but then as you scale it becomes more efficient, while the on-site, organic approach can quickly fizzle out.
Now that we're a much larger team spanning the globe, we've learned a lot at GitLab about how to collaborate effectively and strenghthen our culture, all while working remote. 
Here's how we make it work.

### We facilitate informal communication

Making social connections with coworkers is important to building trust within your organization. One must be **intentional** about designing **informal** communication when it cannot happen more organically in an office. All-remote companies need to facilitate these interactions for their teams. Here are some of the ways we do that at GitLab:

- [Company call](/handbook/communication/#company-call): A daily team video call with an agenda where everyone is free to add subjects they'd like to discuss with the whole company.
- [Breakout calls](/handbook/communication/#breakout-call): Following the company call, everyone breaks out into small groups for 10-15 minutes to talk about non-work-related topics.
- [Contribute Unconference](/company/culture/contribute/): An in-person, week-long event where we bring the entire company together in one location to get to know each other better.
- [Group conversations](/handbook/people-operations/group-conversations/): Four times a week the company gets together virtually to discuss an area of the business. Slides are provided for context but not presented.
- Coffee chats: More details below.
- Coworking calls: More details below. 
- Social hours: Informal social calls organized within our immediate teams to get to know each other on a more personal level. 
- [Visiting grants](/handbook/incentives/#visiting-grant): This travel stipend encourages team members to visit each other by covering transportation costs up to $150 per person they visit.
- Local meetups: Co-located team members are encouraged to organize their own meetups, whether it's a coworking space or getting dinner together. 
- [CEO house](/handbook/ceo/#house): Team members can get together in Utrecht, Netherlands, at the CEO's AirBnB, free of charge. 
- [Slack](/handbook/communication/#slack): We use Slack channels for informal communications throughout the company, whether it's a team-specific channel or a channel dedicated to sharing vacation photos with other team members. 

>  **"I’ve been given a tour of team members’ new houses, admired their Christmas decorations, squealed when their pets and kids make an appearance and watched them preparing dinner – glimpses into the personal lives of my colleagues that I’ve never had in any office job."** - [Rebecca](https://about.gitlab.com/company/team/#rebecca), Managing Editor, GitLab

#### Coffee chats

We understand that working remotely leads to mostly work-related conversations
with fellow team members, so everyone at GitLab is encouraged to dedicate **a few hours a week**
to having social calls with anyone in the company. 

It's a great chance to get to know who you work with,
talk about everyday things and share a coffee, tea, or your favorite beverage. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. 

The Coffee Chats are different from the
[Random Room](/handbook/communication/#random-room) video chat, they are meant to give you the option
to have 1x1 calls with specific teammates who you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

Team members can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee chat. The "Donut" bot will automatically send a message to two people in the channel every other Monday. 
Please schedule a chat together, and Donut will follow up for feedback.
Of course, you can also directly reach out to your fellow GitLab team-members to schedule a coffee chat in the #donut_be_strangers Slack channel or via direct message.

#### Coworking calls

These video calls are scheduled working sessions on Zoom where team members can work through challenging tasks with a coworker, or simply hang out while each person works on their own tasks. 
This recreates a productive working session you might have in person in a traditional office setting, but from the comfort of your own desk. 
Want to try advanced mode? Screen share as you work together (keeping in mind any confidentiality issues).

### We default to asynchronous communication and documenting everything

From opening issues to discuss new ideas to recording "Ask me anything" (AMA) sessions with our leaders, we're intentional in our approach to documenting everything and sharing openly.
This level of documentation means we can [communicate asynchronously](https://about.gitlab.com/handbook/communication/) as much as possible, allowing us to collaborate during whatever working hours are best. 
Because we're all located across many time zones, this communication style is an important part of how we operate as a flexible, efficient team. 

>  **"While some companies allow their employees to work remotely, it isn’t always flexible. With the asynchronous model, it gives me the freedom to design my day around my peak productivity hours and take care of general life stuff."** - [Sarah](https://about.gitlab.com/company/team/#sdaily), Digital Marketing Programs Manager, GitLab 

Remote work is also what led to the development of our publicly viewable [handbook](/handbook/), which captures everything you'd need to know about the company. 
If you can't tell, we like efficiency and don't like having to explain things twice. 

This helps with onboarding new team members, because everything they need to know is in one place. 
We also have an extensive [onboarding template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) and we host a [GitLab 101](/company/culture/gitlab-101/) for new hires to ask questions.
#### Docs beat whiteboards

Often we are asked, "But how do you whiteboard without everyone physically together?" We use Google Docs for collaboration. Every meeting has a Google Doc for the agenda and for documenting discussion, decisions, and actions. Everyone in the meeting adds notes at the same time. We literally even finish one another's sentences some times. By brainstorming in text, instead of drawings, we are forced to clearly articulate proposals, designs, or ideas, with less variance in interpretations. A picture may be worth a thousand words, but it is open to as many interpretations are there are people viewing it. In Google Docs, we use indentations to drill deeper into a given topic. This method retains context for comments, discussions, and ideas.

#### Docs instead of watercoolers

Documentation also helps with transparency, critical to remote work. While decisions made onsite around watercoolers are more familiar to traditional work places, input is limited by who are present and those not present feel left out. The GitLab way of work not only facilitates remote work, but is also more inclusive. By documenting everything, no one is left out of the conversation and diverse perspectives can be included, not only from GitLab team members but also from customers and community contributors.

### We're transparent about hiring and compensation principles

#### Compensation

As an open, all-remote organization with team members and candidates around the world, it's important that we're as transparent as possible about our [compensation principles](https://about.gitlab.com/handbook/people-operations/global-compensation/#compensation-principles). 
That's why we created a [compensation calculator](https://about.gitlab.com/handbook/people-operations/global-compensation/calculator/) that helps determine compensation for more than 200 regions globally. 

Take a look at [how our compensation calculator works](https://about.gitlab.com/handbook/people-operations/global-compensation/#compensation-calculator), and learn more about [why we pay local rates](https://about.gitlab.com/2019/02/28/why-we-pay-local-rates/). 

#### Country hiring guidelines

Hiring globally as an all-remote company has many [advantages](https://about.gitlab.com/company/culture/all-remote/#advantages), including bringing diversity to our team. But it also poses unique challenges because many countries differ in their rules, laws, and regulations. 
Find out more about how we handle this in our [country hiring guidelines](https://about.gitlab.com/jobs/faq/#country-hiring-guidelines) in the handbook. 

## Sharing what we've learned

Check out these interviews with GitLab CEO Sid Sijbrandij to learn more about how our all-remote company works.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tSp5se9BudA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pDU8lxh1-6U" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6mZqzK_40FE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Tips for leaders and other companies

Here are some things that will help make your all-remote team successful:

- Don't require people to have consistent set working hours or say when they're working.
- Don't encourage or celebrate long working hours or working on weekends.
- Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process.
- Encourage non-work related communication (talking about private life on a team call).
- Encourage group video calls for [bonding](/2015/04/08/the-remote-manifesto/).
- Encourage [video calls](/2015/04/08/the-remote-manifesto/) between people (as part of onboarding).
- Host periodic summits with the whole company to get to know each other in an informal setting.
- Encourage [teamwork and saying thanks](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
- Assign new hires a buddy so they have someone to reach out to in their first weeks.
- Allow everyone in the company to view and edit every document.
- Consider every document a draft, don't wait to share until it's done.
- Encourage people to write down all information.

>  **“I work closely with our executive team here, and they have been so supportive and encouraging when family-related conflicts arise. They are constantly reminding me that “family first” is our mantra, and give me ease of mind to take time away when needed. Sid, our co-founder and CEO, told me if it’s a beautiful day out and I just want to go enjoy it, I should do that. Moments like these make me so proud to be a part of the GitLab team."** 
[- Cheri, Manager, Executive Assistant](https://about.gitlab.com/company/team/#cheriholmes)

## Tips for employees

Arguably the biggest advantage of working remotely and asynchronously is the
flexibility it provides. This makes it easy to combine work with your
personal life, although it might be difficult to find the right balance. 
Here are some tips that might help:

- Designate a space that's used solely for work so that you can mentally switch from home to office. Don't have a separate room to use as an office? Consider using a screen or partition to physically divide the space.
- Make sure you have the equipment you need to be productive (for GitLab team members, here's a helpful [guide.](https://about.gitlab.com/handbook/spending-company-money/)).
- Take small breaks throughout your working hours to keep your creativity or productivity going. 
- Explicitly plan your time off so that you'll intentionally take time for yourself.
- When you're not working, disconnect by turning off Slack and closing down your email client. At GitLab, this only works if all team members abide by the [communication guidelines](/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus).
- Enable the "working hours" feature on your calendar so that team members in other time zones know not to schedule meetings during times when you're not working.
- Join a coworking space or meet up with someone else who works remotely to cowork in person. 
- If you worked at an office before, you might be missing your default group of coworkers at lunch. Now that you have the flexibility to choose what you do with that time, reach out to a friend and ask them to lunch. 
- Ask your coworkers to share tips and suggestions about what remote work tactics work best for them. 

### Create an ergonomic workspace

The goal of [office ergonomics](http://ergo-plus.com/office-ergonomics-10-tips-to-help-you-avoid-fatigue/) is to design your office work station so that it fits you and allows for a comfortable working environment for maximum productivity and efficiency. 
Since we all work from home, GitLab wants each team member to have the [supplies](/handbook/spending-company-money/) and knowledge they need to create an ergonomic home office.

Here are some tips from the [Mayo Clinic](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/office-ergonomics/art-20046169) how on how arrange your work station. 

- **Chair**: Choose a chair that supports your spinal curves. Adjust the height of your chair so that your feet rest flat on the floor or on a footrest and your thighs are parallel to the floor. Adjust armrests so your arms gently rest on them with your shoulders relaxed.
- **Keyboard and mouse:** Place your mouse within easy reach and on the same surface as your keyboard. While typing or using your mouse, keep your wrists straight, your upper arms close to your body, and your hands at or slightly below the level of your elbows. Use keyboard shortcuts to reduce extended mouse use. If possible, adjust the sensitivity of the mouse so you can use a light touch to operate it. Alternate the hand you use to operate the mouse by moving the mouse to the other side of your keyboard. Keep regularly used objects close to your body to minimize reaching. Stand up to reach anything that can't be comfortably reached while sitting.
- **Telephone**: If you frequently talk on the phone and type or write at the same time, place your phone on speaker or use a headset rather than cradling the phone between your head and neck.
- **Footrest**: If your chair is too high for you to rest your feet flat on the floor — or the height of your desk requires you to raise the height of your chair — use a footrest. If a footrest is not available, try using a small stool or a stack of sturdy books instead.
- **Desk**: Under the desk, make sure there's clearance for your knees, thighs and feet. If the desk is too low and can't be adjusted, place sturdy boards or blocks under the desk legs. If the desk is too high and can't be adjusted, raise your chair. Use a footrest to support your feet as needed. If your desk has a hard edge, pad the edge or use a wrist rest. Don't store items under your desk. GitLab recommends having an adjustable standing desk to avoid any issues.
- **Monitor**: Place the monitor directly in front of you, about an arm's length away. The top of the screen should be at or slightly below eye level. The monitor should be directly behind your keyboard. If you wear bifocals, lower the monitor an additional 1 to 2 inches for more comfortable viewing. Place your monitor so that the brightest light source is to the side.

Note: If you develop any pains which you think might be related to your working position, please visit a doctor.

### Dedicate time for health and fitness

It's sometimes hard to remember to stay active when you work from home. Here are some tips that might help:

- Try to step away from your computer and stretch your body every hour.
- Avoid "Digital Eye Strain" by following the [20-20-20 Rule](https://www.healthline.com/health/eye-health/20-20-20-rule#definition). Every 20 minutes look into the distance (at least 20 feet/6 meters) for 20 seconds.
- Use an app - there are some that will remind you to take a break or help you with your computer posture:
    - [PostureMinder](http://www.postureminder.co.uk/)(Windows)
    - [Time Out](https://itunes.apple.com/us/app/time-out-break-reminders/id402592703?mt=12)(macOS)
    - [Awareness](http://iamfutureproof.com/tools/awareness/)(macOS)
    - [SafeEyes](http://slgobinath.github.io/SafeEyes/)(GNU/Linux)
- Move every day
    - Go for a walk or do a short excersise for at least 15 minutes a day.
    - Do something active that can be done within a short amount of time like rope jumping, lifting kettlebells, push-ups or sit-ups. It might also help to split the activity into multiple shorter sessions. You can use an app that helps you with the workout, e.g., [7 minute workout](https://7minuteworkout.jnj.com/).
- Try to create a repeatable schedule that is consistent and try to make a habit out of it. Make sure you enjoy the activity.
- It can also help to create a goal or challenge for yourself, e.g. registering for a race.
- Eat less refined sugar and simple carbs, and eat complex carbs instead. Try to eat more vegetables. Don't go to the kitchen to eat something every 15 minutes (it’s a trap!). Keep junk food out of your house.
- Have a water bottle with you at your desk. You will be more inclined to drink if it's available at all times.
- Try to get enough sleep at night and take a nap during the day if you need one.

At GitLab, we want to ensure each team member takes care of themselves and dedicates time to stay healthy. You can also join the Slack channel `fitlab` to discuss your tips, challenges, results, etc. with other team members.

## Contribute to this page

We recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community. 
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.

----

Return to the main [all-remote page](/company/culture/all-remote/).
